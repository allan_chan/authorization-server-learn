package com.xueliman.iov.resource.controller;

import com.xueliman.iov.cloud.framework.web.exception.ApiException;
import com.xueliman.iov.resource.data.dto.TestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * @author zxg
 */
@Slf4j
@RestController
public class ResourceController {

    /**
     * 测试Spring Authorization Server，必须指定客户端
     */
    @PreAuthorize("hasAuthority('SCOPE_message.read')")
    @GetMapping("/getTest")
    public String getTest(){
        return "getTest";
    }

    /**
     * 默认登录成功跳转页为 /  防止404状态
     *
     * @return the map
     */
    @GetMapping("/")
    public Map<String, String> index() {
        return Collections.singletonMap("msg", "login success!");
    }

    @GetMapping("/getResourceTest")
    public String getResourceTest(){
        return ("这是resource的测试方法 getResourceTest()");
    }

    @GetMapping("/getResourceTest2")
    public String getResourceTest2(){
        throw new ApiException("手动异常");
//        return "这是resource的测试方法 getResourceTest2()";
    }

    @PostMapping("/getResourceTest3")
    public Integer getResourceTest3(@RequestBody TestDTO testDTO){
        throw new ApiException("手动异常");
//        return "这是resource的测试方法 getResourceTest2()";
    }

}
